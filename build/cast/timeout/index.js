"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.timeout

var Timeout = function Timeout() {
  _classCallCheck(this, Timeout);
};

Timeout.requestSession = 15000;
Timeout.leaveSession = 3000;
Timeout.stopSession = 3000;
Timeout.setReceiverVolume = 3000;
Timeout.sendCustomMessage = 3000;
exports.default = Timeout;