"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.timeout

var Timeout = function Timeout() {
  _classCallCheck(this, Timeout);
};

Timeout.editTracksInfo = 0;
Timeout.getStatus = 0;
Timeout.load = 0;
Timeout.pause = 0;
Timeout.play = 0;
Timeout.queue = 0;
Timeout.seek = 0;
Timeout.setVolume = 0;
Timeout.stop = 0;
exports.default = Timeout;