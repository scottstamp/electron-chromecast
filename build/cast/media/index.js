'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _EditTracksInfoRequest = require('./_Classes/EditTracksInfoRequest');

var _EditTracksInfoRequest2 = _interopRequireDefault(_EditTracksInfoRequest);

var _GenericMediaMetadata = require('./_Classes/GenericMediaMetadata');

var _GenericMediaMetadata2 = _interopRequireDefault(_GenericMediaMetadata);

var _GetStatusRequest = require('./_Classes/GetStatusRequest');

var _GetStatusRequest2 = _interopRequireDefault(_GetStatusRequest);

var _LoadRequest = require('./_Classes/LoadRequest');

var _LoadRequest2 = _interopRequireDefault(_LoadRequest);

var _Media = require('./_Classes/Media');

var _Media2 = _interopRequireDefault(_Media);

var _MediaInfo = require('./_Classes/MediaInfo');

var _MediaInfo2 = _interopRequireDefault(_MediaInfo);

var _MovieMediaMetadata = require('./_Classes/MovieMediaMetadata');

var _MovieMediaMetadata2 = _interopRequireDefault(_MovieMediaMetadata);

var _MusicTrackMediaMetadata = require('./_Classes/MusicTrackMediaMetadata');

var _MusicTrackMediaMetadata2 = _interopRequireDefault(_MusicTrackMediaMetadata);

var _PauseRequest = require('./_Classes/PauseRequest');

var _PauseRequest2 = _interopRequireDefault(_PauseRequest);

var _PhotoMediaMetadata = require('./_Classes/PhotoMediaMetadata');

var _PhotoMediaMetadata2 = _interopRequireDefault(_PhotoMediaMetadata);

var _PlayRequest = require('./_Classes/PlayRequest');

var _PlayRequest2 = _interopRequireDefault(_PlayRequest);

var _QueueInsertItemsRequest = require('./_Classes/QueueInsertItemsRequest');

var _QueueInsertItemsRequest2 = _interopRequireDefault(_QueueInsertItemsRequest);

var _QueueItem = require('./_Classes/QueueItem');

var _QueueItem2 = _interopRequireDefault(_QueueItem);

var _QueueLoadRequest = require('./_Classes/QueueLoadRequest');

var _QueueLoadRequest2 = _interopRequireDefault(_QueueLoadRequest);

var _QueueRemoveItemsRequest = require('./_Classes/QueueRemoveItemsRequest');

var _QueueRemoveItemsRequest2 = _interopRequireDefault(_QueueRemoveItemsRequest);

var _QueueReorderItemsRequest = require('./_Classes/QueueReorderItemsRequest');

var _QueueReorderItemsRequest2 = _interopRequireDefault(_QueueReorderItemsRequest);

var _QueueSetPropertiesRequest = require('./_Classes/QueueSetPropertiesRequest');

var _QueueSetPropertiesRequest2 = _interopRequireDefault(_QueueSetPropertiesRequest);

var _QueueUpdateItemsRequest = require('./_Classes/QueueUpdateItemsRequest');

var _QueueUpdateItemsRequest2 = _interopRequireDefault(_QueueUpdateItemsRequest);

var _SeekRequest = require('./_Classes/SeekRequest');

var _SeekRequest2 = _interopRequireDefault(_SeekRequest);

var _StopRequest = require('./_Classes/StopRequest');

var _StopRequest2 = _interopRequireDefault(_StopRequest);

var _TextTrackStyle = require('./_Classes/TextTrackStyle');

var _TextTrackStyle2 = _interopRequireDefault(_TextTrackStyle);

var _Track = require('./_Classes/Track');

var _Track2 = _interopRequireDefault(_Track);

var _TvShowMediaMetadata = require('./_Classes/TvShowMediaMetadata');

var _TvShowMediaMetadata2 = _interopRequireDefault(_TvShowMediaMetadata);

var _VolumeRequest = require('./_Classes/VolumeRequest');

var _VolumeRequest2 = _interopRequireDefault(_VolumeRequest);

var _timeout = require('./timeout');

var _timeout2 = _interopRequireDefault(_timeout);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MediaStatic = function MediaStatic() {
  _classCallCheck(this, MediaStatic);
};

MediaStatic.IdleReason = {
  CANCELLED: 'CANCELLED',
  INTERRUPTED: 'INTERRUPTED',
  FINISHED: 'FINISHED',
  ERROR: 'ERROR'
};
MediaStatic.MediaCommand = {
  PAUSE: 'pause',
  SEEK: 'seek',
  STREAM_VOLUME: 'stream_volume',
  STREAM_MUTE: 'stream_mute'
};
MediaStatic.MetadataType = {
  GENERIC: 'GENERIC',
  MOVIE: 'MOVIE',
  TV_SHOW: 'TV_SHOW',
  MUSIC_TRACK: 'MUSIC_TRACK',
  PHOTO: 'PHOTO'
};
MediaStatic.PlayerState = {
  IDLE: 'IDLE',
  PLAYING: 'PLAYING',
  PAUSED: 'PAUSED',
  BUFFERING: 'BUFFERING'
};
MediaStatic.RepeatMode = {
  OFF: 'REPEAT_OFF',
  ALL: 'REPEAT_ALL',
  SINGLE: 'REPEAT_SINGLE',
  ALL_AND_SHUFFLE: 'REPEAT_ALL_AND_SHUFFLE'
};
MediaStatic.ResumeState = {
  PLAYBACK_START: 'PLAYBACK_START',
  PLAYBACK_PAUSE: 'PLAYBACK_PAUSE'
};
MediaStatic.StreamType = {
  BUFFERED: 'BUFFERED',
  LIVE: 'LIVE',
  OTHER: 'OTHER'
};
MediaStatic.TextTrackEdgeType = {
  NONE: 'NONE',
  OUTLINE: 'OUTLINE',
  DROP_SHADOW: 'DROP_SHADOW',
  RAISED: 'RAISED',
  DEPRESSED: 'DEPRESSED'
};
MediaStatic.TextTrackFontGenericFamily = {
  SANS_SERIF: 'SANS_SERIF',
  MONOSPACED_SANS_SERIF: 'MONOSPACED_SANS_SERIF',
  SERIF: 'SERIF',
  MONOSPACED_SERIF: 'MONOSPACED_SERIF',
  CASUAL: 'CASUAL',
  CURSIVE: 'CURSIVE',
  SMALL_CAPITALS: 'SMALL_CAPITALS'
};
MediaStatic.TextTrackFontStyle = {
  NORMAL: 'NORMAL',
  BOLD: 'BOLD',
  BOLD_ITALIC: 'BOLD_ITALIC',
  ITALIC: 'ITALIC'
};
MediaStatic.TextTrackType = {
  SUBTITLES: 'SUBTITLES',
  CAPTIONS: 'CAPTIONS',
  DESCRIPTIONS: 'DESCRIPTIONS',
  CHAPTERS: 'CHAPTERS',
  METADATA: 'METADATA'
};
MediaStatic.TextTrackWindowType = {
  NONE: 'NONE',
  NORMAL: 'NORMAL',
  ROUNDED_CORNERS: 'ROUNDED_CORNERS'
};
MediaStatic.TrackType = {
  TEXT: 'TEXT',
  AUDIO: 'AUDIO',
  VIDEO: 'VIDEO'
};
MediaStatic.DEFAULT_MEDIA_RECEIVER_APP_ID = 'CC1AD845';
exports.default = MediaStatic;


MediaStatic.EditTracksInfoRequest = _EditTracksInfoRequest2.default;
MediaStatic.GenericMediaMetadata = _GenericMediaMetadata2.default;
MediaStatic.GetStatusRequest = _GetStatusRequest2.default;
MediaStatic.LoadRequest = _LoadRequest2.default;
MediaStatic.Media = _Media2.default;
MediaStatic.MediaInfo = _MediaInfo2.default;
MediaStatic.MovieMediaMetadata = _MovieMediaMetadata2.default;
MediaStatic.MusicTrackMediaMetadata = _MusicTrackMediaMetadata2.default;
MediaStatic.PauseRequest = _PauseRequest2.default;
MediaStatic.PhotoMediaMetadata = _PhotoMediaMetadata2.default;
MediaStatic.PlayRequest = _PlayRequest2.default;
MediaStatic.QueueInsertItemsRequest = _QueueInsertItemsRequest2.default;
MediaStatic.QueueItem = _QueueItem2.default;
MediaStatic.QueueLoadRequest = _QueueLoadRequest2.default;
MediaStatic.QueueRemoveItemsRequest = _QueueRemoveItemsRequest2.default;
MediaStatic.QueueReorderItemsRequest = _QueueReorderItemsRequest2.default;
MediaStatic.QueueSetPropertiesRequest = _QueueSetPropertiesRequest2.default;
MediaStatic.QueueUpdateItemsRequest = _QueueUpdateItemsRequest2.default;
MediaStatic.SeekRequest = _SeekRequest2.default;
MediaStatic.StopRequest = _StopRequest2.default;
MediaStatic.TextTrackStyle = _TextTrackStyle2.default;
MediaStatic.Track = _Track2.default;
MediaStatic.TvShowMediaMetadata = _TvShowMediaMetadata2.default;
MediaStatic.VolumeRequest = _VolumeRequest2.default;

MediaStatic.timeout = _timeout2.default;