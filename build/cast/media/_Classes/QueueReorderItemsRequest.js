'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.QueueReorderItemsRequest

var QueueReorderItemsRequest = function QueueReorderItemsRequest(itemIdsToReorder) {
  _classCallCheck(this, QueueReorderItemsRequest);

  this.customData = {};
  this.type = 'QUEUE_REORDER';
  this.insertBefore = null;
  this.itemIds = itemIdsToReorder;
};

exports.default = QueueReorderItemsRequest;