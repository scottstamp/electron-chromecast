"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.QueueRemoveItemsRequest

var QueueRemoveItemsRequest = function QueueRemoveItemsRequest(itemIdsToRemove) {
  _classCallCheck(this, QueueRemoveItemsRequest);

  this.customData = {};
  this.itemIds = itemIdsToRemove;
};

exports.default = QueueRemoveItemsRequest;