"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.MusicTrackMediaMetadata

var MusicTrackMediaMetadata = function MusicTrackMediaMetadata() {
  _classCallCheck(this, MusicTrackMediaMetadata);

  this.albumArtist = null;
  this.albumName = null;
  this.artist = null;
  this.composer = null;
  this.discNumber = null;
  this.images = [];
  this.metadataType = this.type = 3;
  this.releaseDate = null;
  this.songName = null;
  this.title = null;
  this.trackNumber = null;
};

exports.default = MusicTrackMediaMetadata;