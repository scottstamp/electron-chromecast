'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.MediaInfo

var MediaInfo = function MediaInfo(contentId, contentType) {
  _classCallCheck(this, MediaInfo);

  castConsole.info('new MediaInfo', contentId, contentType);
  this.contentId = contentId;
  this.contentType = contentType;
  this.customData = {};
  this.duration = null;
  this.metadata = null;
  this.streamType = chrome.cast.media.StreamType.BUFFERED;
  this.textTrackStyle = null;
  this.tracks = [];
};

exports.default = MediaInfo;