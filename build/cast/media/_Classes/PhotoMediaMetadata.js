"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.PhotoMediaMetadata

var PhotoMediaMetadata = function PhotoMediaMetadata() {
  _classCallCheck(this, PhotoMediaMetadata);

  this.artist = null;
  this.creationDateTime = null;
  this.height = null;
  this.images = [];
  this.latitude = null;
  this.location = null;
  this.longitude = null;
  this.metadataType = null;
  this.title = null;
  this.width = null;
};

exports.default = PhotoMediaMetadata;