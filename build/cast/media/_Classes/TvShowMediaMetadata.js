"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.TvShowMediaMetadata

var TvShowMediaMetadata = function TvShowMediaMetadata() {
  _classCallCheck(this, TvShowMediaMetadata);

  this.episode = null;
  this.images = [];
  this.metadataType = null;
  this.originalAirdate = null;
  this.season = null;
  this.seriesTitle = null;
  this.title = null;
};

exports.default = TvShowMediaMetadata;