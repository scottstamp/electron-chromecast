'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.LoadRequest

var LoadRequest = function LoadRequest(mediaInfo) {
  _classCallCheck(this, LoadRequest);

  castConsole.info('new LoadRequest', mediaInfo);
  this.activeTrackIds = [];
  this.autoplay = false;
  this.currentTime = 0;
  this.customData = {};
  this.media = mediaInfo;
};

exports.default = LoadRequest;