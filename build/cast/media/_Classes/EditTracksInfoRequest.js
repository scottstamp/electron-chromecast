"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.EditTracksInfoRequest

var EditTracksInfoRequest = function EditTracksInfoRequest(opt_activeTrackIds, opt_textTrackStyle) {
  _classCallCheck(this, EditTracksInfoRequest);

  this.activeTrackIds = opt_activeTrackIds || null;
  this.textTrackStyle = opt_textTrackStyle || null;
};

exports.default = EditTracksInfoRequest;