"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.PauseRequest

var PauseRequest = function PauseRequest() {
  _classCallCheck(this, PauseRequest);

  this.customData = {};
};

exports.default = PauseRequest;