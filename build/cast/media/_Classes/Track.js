"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Track

var Track = function Track(trackId, trackType) {
  _classCallCheck(this, Track);

  this.customData = {};
  this.language = null;
  this.name = null;
  this.subtype = null;
  this.trackContentId = null;
  this.trackContentType = null;
  this.trackId = trackId;
  this.type = trackType;
};

exports.default = Track;