"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.GenericMediaMetadata

var GenericMediaMetadata = function GenericMediaMetadata() {
  _classCallCheck(this, GenericMediaMetadata);

  this.images = [];
  this.metadataType = null;
  this.releaseDate = null;
  this.subtitle = null;
  this.title = null;
};

exports.default = GenericMediaMetadata;