"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.QueueInsertItemsRequest

var QueueInsertItemsRequest = function QueueInsertItemsRequest(itemsToInsert) {
  _classCallCheck(this, QueueInsertItemsRequest);

  this.customData = {};
  this.insertBefore = null;
  this.items = itemsToInsert;
};

exports.default = QueueInsertItemsRequest;