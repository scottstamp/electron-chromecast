"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.SeekRequest

var SeekRequest = function SeekRequest() {
  _classCallCheck(this, SeekRequest);

  this.currentTime = null;
  this.customData = {};
  this.resumeState = null;
};

exports.default = SeekRequest;