"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.QueueLoadRequest

var QueueLoadRequest = function QueueLoadRequest(items) {
  _classCallCheck(this, QueueLoadRequest);

  this.customData = {};
  this.items = items;
  this.repeatMode = chrome.cast.media.RepeatMode.OFF;
  this.startIndex = 0;
};

exports.default = QueueLoadRequest;