'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media
var noop = function noop() {};

var Media = function () {
  function Media(sessionId, mediaSessionId, _channel) {
    var _this = this;

    _classCallCheck(this, Media);

    this.activeTrackIds = [];
    this.currentItemId = 1;
    this.customData = {};
    this.currentTime = 0;
    this.idleReason = null;
    this.items = [];
    this.loadingItemId = null;
    this.media = null;
    this.mediaSessionId = mediaSessionId;
    this.playbackRate = 1;
    this.playerState = chrome.cast.media.PlayerState.PAUSED;
    this.preloadedItemId = null;
    this.repeatMode = chrome.cast.media.RepeatMode.OFF;
    this.sessionId = sessionId;
    this.supportedMediaCommands = [chrome.cast.media.MediaCommand.PAUSE, chrome.cast.media.MediaCommand.SEEK, chrome.cast.media.MediaCommand.STREAM_VOLUME, chrome.cast.media.MediaCommand.STREAM_MUTE];
    this.volume = new chrome.cast.Volume();

    this.channel = _channel;
    this.channel.on('message', function (data) {
      if (data && data.type === 'MEDIA_STATUS' && data.status && data.status.length > 0) {
        castConsole.error('Update MEDIA based on:', data);
        var status = data.status[0];
        _this.currentTime = status.currentTime;
        _this._lastCurrentTime = Date.now() / 1000;
        _this.customData = status.customData;
        _this.volume = new chrome.cast.Volume(status.volume.level, status.volume.muted);
        if (status.media) {
          _this.media = status.media;
        }
        if (status.items) {
          _this.items = status.items;
        }
        if (status.mediaSessionId) {
          _this.mediaSessionId = status.mediaSessionId;
        }
        _this.playbackRate = status.playbackRate;
        _this.playerState = status.playerState;
        _this.repeatMode = status.repeatMode;
        // currentTime, volume, metadata, playbackRate, playerState, customData

        _this._updateHooks.forEach(function (hookFn) {
          return hookFn(true);
        });
      }
    });

    this._updateHooks = [];
  }

  _createClass(Media, [{
    key: '_sendMediaMessage',
    value: function _sendMediaMessage(message, successCallback, errorCallback) {
      message.mediaSessionId = this.mediaSessionId; // eslint-disable-line
      message.requestId = 0; // eslint-disable-line
      message.sessionId = this.sessionId; // eslint-disable-line
      message.customData = null; // eslint-disable-line
      try {
        this.channel.send(message);
        castConsole.info(message);
      } catch (e) {
        castConsole.error(e);
        errorCallback(new chrome.cast.Error(chrome.cast.ErrorStatus.SESSION_ERROR));
      }
      successCallback();
    }
  }, {
    key: '_hydrate',
    value: function _hydrate(mediaData) {
      if (typeof mediaData.status !== 'undefined' && Array.isArray(mediaData.status) && mediaData.status.length > 0) {
        Object.assign(this, mediaData.status[0]);
      }
    }
  }, {
    key: 'addUpdateListener',
    value: function addUpdateListener(listener) {
      // https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#addUpdateListener
      this._updateHooks.push(listener);
    }
  }, {
    key: 'editTracksInfo',
    value: function editTracksInfo(editTracksInfoRequest, successCallback, errorCallback) {
      castConsole.info('editTracksInfoRequest', editTracksInfoRequest, errorCallback);
      // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#editTracksInfo
    }
  }, {
    key: 'getEstimatedTime',
    value: function getEstimatedTime() {
      // https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#getEstimatedTime
      if (!this.currentTime) return 0;
      return this.currentTime + (Date.now() / 1000 - this._lastCurrentTime);
    }
  }, {
    key: 'getStatus',
    value: function getStatus(getStatusRequest, successCallback, errorCallback) {
      castConsole.info('getStatusRequest', getStatusRequest, errorCallback);
      this._sendMediaMessage({ type: 'MEDIA_GET_STATUS' }, successCallback || noop, errorCallback || noop);
    }
  }, {
    key: 'pause',
    value: function pause(pauseRequest, successCallback, errorCallback) {
      // https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#pause
      castConsole.info('pause', pauseRequest);
      this._sendMediaMessage({ type: 'PAUSE' }, successCallback || noop, errorCallback || noop);
    }
  }, {
    key: 'play',
    value: function play(playRequest, successCallback, errorCallback) {
      // https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#play
      this._sendMediaMessage({ type: 'PLAY' }, successCallback || noop, errorCallback || noop);
    }
  }, {
    key: 'queueAppendItem',
    value: function queueAppendItem(item, successCallback, errorCallback) {
      castConsole.info('queueAppendItem', item, errorCallback);
      // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#queueAppendItem
    }
  }, {
    key: 'queueInsertItems',
    value: function queueInsertItems(queueInsertItemsRequest, successCallback, errorCallback) {
      // eslint-disable-line no-unused-vars
      castConsole.info('queueInsertItems', queueInsertItemsRequest);
      // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#queueInsertItems
    }
  }, {
    key: 'queueJumpToItem',
    value: function queueJumpToItem(itemId, successCallback, errorCallback) {
      // eslint-disable-line no-unused-vars
      this._sendMediaMessage({ type: 'QUEUE_UPDATE', currentItemId: itemId }, successCallback || noop, errorCallback || noop);
      // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#queueJumpToItem
    }
  }, {
    key: 'queueMoveItemToNewIndex',
    value: function queueMoveItemToNewIndex(itemId, newIndex, successCallback, errorCallback) {
      // eslint-disable-line no-unused-vars
      castConsole.info('queueMoveItemToNewIndex', itemId, newIndex);
      // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#queueMoveItemToNewIndex
    }
  }, {
    key: 'queueNext',
    value: function queueNext(successCallback, errorCallback) {
      // eslint-disable-line no-unused-vars
      this._sendMediaMessage({ type: 'QUEUE_UPDATE', jump: 1 }, successCallback || noop, errorCallback || noop);
      // https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#queueNext
    }
  }, {
    key: 'queuePrev',
    value: function queuePrev(successCallback, errorCallback) {
      // eslint-disable-line no-unused-vars
      this._sendMediaMessage({ type: 'QUEUE_UPDATE', jump: -1 }, successCallback || noop, errorCallback || noop);
      // https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#queuePrev
    }
  }, {
    key: 'queueRemoveItem',
    value: function queueRemoveItem(itemId, successCallback, errorCallback) {
      // eslint-disable-line no-unused-vars
      castConsole.info('queueRemoveItem', itemId);
      // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#queueRemoveItem
    }
  }, {
    key: 'queueReorderItems',
    value: function queueReorderItems(queueReorderItemsRequest, successCallback, errorCallback) {
      // eslint-disable-line no-unused-vars
      castConsole.info('queueReorderItems', queueReorderItemsRequest);
      // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#queueReorderItems
    }
  }, {
    key: 'queueSetRepeatMode',
    value: function queueSetRepeatMode(repeatMode, successCallback, errorCallback) {
      // eslint-disable-line no-unused-vars
      this._sendMediaMessage({ type: 'QUEUE_UPDATE', repeatMode: repeatMode }, successCallback || noop, errorCallback || noop);
      // https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#queueSetRepeatMode
    }
  }, {
    key: 'queueUpdateItems',
    value: function queueUpdateItems(queueUpdateItemsRequest, successCallback, errorCallback) {
      // eslint-disable-line no-unused-vars
      castConsole.info('queueUpdateItems', queueUpdateItemsRequest);
      // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#queueUpdateItems
    }
  }, {
    key: 'removeUpdateListener',
    value: function removeUpdateListener(listener) {
      var updateListenerIndex = this._updateHooks.push(listener);
      if (updateListenerIndex > -1) {
        this._updateHooks.splice(updateListenerIndex, 1);
      }
      // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#removeUpdateListener
    }
  }, {
    key: 'seek',
    value: function seek(seekRequest, successCallback, errorCallback) {
      // eslint-disable-line no-unused-vars
      this._sendMediaMessage({ type: 'SEEK', currentTime: seekRequest.currentTime }, successCallback || noop, errorCallback || noop);
      // https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#seek
    }
  }, {
    key: 'setVolume',
    value: function setVolume(volumeRequest, successCallback, errorCallback) {
      // eslint-disable-line no-unused-vars
      this._sendMediaMessage({ type: 'SET_VOLUME', volume: volumeRequest.volume }, successCallback || noop, errorCallback || noop);
      // https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#setVolume
    }
  }, {
    key: 'stop',
    value: function stop(stopRequest, successCallback, errorCallback) {
      // https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#stop
      this._sendMediaMessage({ type: 'STOP' }, successCallback || noop, errorCallback || noop);
    }
  }, {
    key: 'supportsCommand',
    value: function supportsCommand(command) {
      castConsole.info('supportsCommand', command);
      // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.Media#supportsCommand
    }
  }]);

  return Media;
}();

exports.default = Media;