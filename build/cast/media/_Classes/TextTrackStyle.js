"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.TextTrackStyle

var TextTrackStyle = function TextTrackStyle() {
  _classCallCheck(this, TextTrackStyle);

  this.backgroundColor = null;
  this.customData = {};
  this.edgeColor = null;
  this.edgeType = null;
  this.fontFamily = null;
  this.fontGenericFamily = null;
  this.fontScale = null;
  this.fontStyle = null;
  this.foregroundColor = null;
  this.windowColor = null;
  this.windowRoundedCornerRadius = null;
  this.windowType = null;
};

exports.default = TextTrackStyle;