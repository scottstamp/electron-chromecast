'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.QueueSetPropertiesRequest

var QueueSetPropertiesRequest = function QueueSetPropertiesRequest() {
  _classCallCheck(this, QueueSetPropertiesRequest);

  this.type = 'QUEUE_UPDATE';
  this.customData = {};
  this.repeatMode = null;
  this.sessionId = null;
  this.requestId = null;
};

exports.default = QueueSetPropertiesRequest;