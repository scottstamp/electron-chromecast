'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.GetStatusRequest

var GetStatusRequest = function GetStatusRequest() {
  _classCallCheck(this, GetStatusRequest);

  castConsole.info('GetStatusRequest');
  this.customData = {};
};

exports.default = GetStatusRequest;