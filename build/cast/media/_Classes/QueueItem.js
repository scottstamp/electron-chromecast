"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.QueueItem

var QueueItem = function QueueItem(mediaInfo) {
  _classCallCheck(this, QueueItem);

  this.activeTrackIds = [];
  this.autoplay = false;
  this.customData = {};
  this.itemId = null;
  this.media = mediaInfo;
  this.preloadTime = 10;
  this.startTime = 0;
};

exports.default = QueueItem;