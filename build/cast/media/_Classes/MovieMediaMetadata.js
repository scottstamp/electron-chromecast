"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.media.MovieMediaMetadata

var MovieMediaMetadata = function MovieMediaMetadata() {
  _classCallCheck(this, MovieMediaMetadata);

  this.images = [];
  this.metadataType = null;
  this.releaseDate = null;
  this.studio = null;
  this.subtitle = null;
  this.title = null;
};

exports.default = MovieMediaMetadata;