'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _ApiConfig = require('./_Classes/ApiConfig');

var _ApiConfig2 = _interopRequireDefault(_ApiConfig);

var _Error = require('./_Classes/Error');

var _Error2 = _interopRequireDefault(_Error);

var _Image = require('./_Classes/Image');

var _Image2 = _interopRequireDefault(_Image);

var _nodeMdnsEasy = require('node-mdns-easy');

var _nodeMdnsEasy2 = _interopRequireDefault(_nodeMdnsEasy);

var _Receiver = require('./_Classes/Receiver');

var _Receiver2 = _interopRequireDefault(_Receiver);

var _ReceiverDisplayStatus = require('./_Classes/ReceiverDisplayStatus');

var _ReceiverDisplayStatus2 = _interopRequireDefault(_ReceiverDisplayStatus);

var _SenderApplication = require('./_Classes/SenderApplication');

var _SenderApplication2 = _interopRequireDefault(_SenderApplication);

var _Session = require('./_Classes/Session');

var _Session2 = _interopRequireDefault(_Session);

var _SessionRequest = require('./_Classes/SessionRequest');

var _SessionRequest2 = _interopRequireDefault(_SessionRequest);

var _Volume = require('./_Classes/Volume');

var _Volume2 = _interopRequireDefault(_Volume);

var _media = require('./media');

var _media2 = _interopRequireDefault(_media);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var mdns = new _nodeMdnsEasy2.default();
var browser = false;

// DEV: Global config variables
var globalApiConfig = void 0;
var receiverList = [];
var receiverListeners = [];
var sessions = [];

var Cast = function Cast() {
  _classCallCheck(this, Cast);
};

Cast.AutoJoinPolicy = {
  TAB_AND_ORIGIN_SCOPED: 'TAB_AND_ORIGIN_SCOPED',
  ORIGIN_SCOPED: 'ORIGIN_SCOPED',
  PAGE_SCOPED: 'PAGE_SCOPED'
};
Cast.Capability = {
  VIDEO_OUT: 'VIDEO_OUT',
  VIDEO_IN: 'VIDEO_IN',
  AUDIO_OUT: 'AUDIO_OUT',
  AUDIO_IN: 'AUDIO_IN'
};
Cast.DefaultActionPolicy = {
  CREATE_SESSION: 'CREATE_SESSION',
  CAST_THIS_TAB: 'CAST_THIS_TAB'
};
Cast.ErrorCode = {
  CANCEL: 'CANCEL',
  TIMEOUT: 'TIMEOUT',
  API_NOT_INITIALIZED: 'API_NOT_INITIALIZED',
  INVALID_PARAMETER: 'INVALID_PARAMETER',
  EXTENSION_NOT_COMPATIBLE: 'EXTENSION_NOT_COMPATIBLE',
  EXTENSION_MISSING: 'EXTENSION_MISSING',
  RECEIVER_UNAVAILABLE: 'RECEIVER_UNAVAILABLE',
  SESSION_ERROR: 'SESSION_ERROR',
  CHANNEL_ERROR: 'CHANNEL_ERROR',
  LOAD_MEDIA_FAILED: 'LOAD_MEDIA_FAILED'
};
Cast.isAvailable = true;
Cast.ReceiverAction = {
  CAST: 'CAST',
  STOP: 'STOP'
};

Cast.ReceiverActionListener = function () {
  function _class() {
    _classCallCheck(this, _class);
  }

  return _class;
}();

Cast.ReceiverAvailability = {
  AVAILABLE: 'AVAILABLE',
  UNAVAILABLE: 'UNAVAILABLE'
};
Cast.ReceiverType = {
  CAST: 'CAST',
  HANGOUT: 'HANGOUT',
  CUSTOM: 'CUSTOM'
};
Cast.SenderPlatform = {
  CHROME: 'CHROME',
  IOS: 'IOS',
  ANDROID: 'ANDROID'
};
Cast.SessionStatus = {
  CONNECTED: 'CONNECTED',
  DISCONNECTED: 'DISCONNECTED',
  STOPPED: 'STOPPED'
};
Cast.VERSION = [1, 2];

Cast.addReceiverActionListener = function (listener) {
  // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast.html#.addReceiverActionListener
  receiverListeners.push(listener);
};

Cast.initialize = function (apiConfig, successCallback, errorCallback) {
  if (globalApiConfig) {
    // DEV: The chromecast API has already been initialzed
    errorCallback(new chrome.cast.Error(chrome.cast.ErrorCode.INVALID_PARAMETER));
    return;
  }
  globalApiConfig = apiConfig;

  if (receiverList.length > 0) {
    globalApiConfig.receiverListener(chrome.cast.ReceiverAvailability.AVAILABLE);
  } else {
    globalApiConfig.receiverListener(chrome.cast.ReceiverAvailability.UNAVAILABLE);
  }
  successCallback();
};

Cast.logMessage = function (message) {
  castConsole.info('CAST MSG:', message); // eslint-disable-line
};

Cast.removeReceiverActionListener = function (listener) {
  // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast#.removeReceiverActionListener
  castConsole.log(listener);
};

Cast.requestSession = function (successCallback, errorCallback, opt_sessionRequest) {
  // eslint-disable-line
  // TODO: Utilize passed in opt_sessionRequest if present, remove disable line
  var id = sessions.length;

  if (receiverList.length === 0) {
    errorCallback(new chrome.cast.Error(chrome.cast.ErrorCode.RECEIVER_UNAVAILABLE));
  } else {
    global.requestHandler(receiverList).then(function (chosenDevice) {
      var createSession = function createSession() {
        var session = new chrome.cast.Session(id, globalApiConfig.sessionRequest.appId, chosenDevice.friendlyName, [], chosenDevice, successCallback);
        sessions.push(session);
      };
      // If we already have a session, terminate the old one and then start
      // the new one
      if (sessions.length && sessions[sessions.length - 1].status !== chrome.cast.SessionStatus.STOPPED) {
        sessions[sessions.length - 1].stop(function () {
          setTimeout(createSession, 0);
        }, function () {});
      } else {
        createSession();
      }
    }).catch(function (message) {
      if (message === Cast.ReceiverAction.STOP && sessions.length) {
        sessions[sessions.length - 1].stop(function () {}, function () {});
      }
      return errorCallback(new chrome.cast.Error(chrome.cast.ErrorCode.CANCEL));
    });
  }
};

Cast.requestSessionById = function (sessionId) {
  // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast#.requestSessionById
  castConsole.log('Get Session', sessionId);
};

Cast.setCustomReceivers = function (receivers, successCallback, errorCallback) {
  // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast#.setCustomReceivers
  castConsole.log(receivers, successCallback, errorCallback);
};

Cast.setReceiverDisplayStatus = function (receiver, successCallback, errorCallback) {
  // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast#.setReceiverDisplayStatus
  castConsole.log(receiver, successCallback, errorCallback);
};

Cast.unescape = function (escaped) {
  return unescape(escaped);
};

Cast.startBrowsing = function () {
  if (browser !== false) {
    browser.stop();
    receiverList = [];
    // Delay unvailability callback if we found receivers in the next 2s (prevent chromecast button flickering)
    setTimeout(function () {
      if (receiverList.length === 0 && globalApiConfig && globalApiConfig.receiverListener) globalApiConfig.receiverListener(chrome.cast.ReceiverAvailability.UNAVAILABLE);
    }, 2000);
  }
  browser = mdns.createBrowser(mdns.getLibrary().tcp('googlecast'));
  browser.on('serviceUp', function (service) {
    var receiver = new chrome.cast.Receiver(service.txtRecord.id, service.txtRecord.fn);

    receiver.ipAddress = service.addresses[0];
    receiver.service_fullname = service.fullname;
    receiver.port = service.port;
    var newReceiverList = receiverList;
    newReceiverList.push(receiver);
    receiverList = _lodash2.default.uniqBy(newReceiverList, _lodash2.default.property('service_fullname'));
    /**
    Service object
    {
      interfaceIndex: 4,
      name: 'somehost',
      networkInterface: 'en0',
      type: {name: 'http', protocol: 'tcp', subtypes: []},
      replyDomain: 'local.',
      fullname: 'somehost._http._tcp.local.',
      host: 'somehost.local.',
      port: 4321,
      addresses: [ '10.1.1.50', 'fe80::21f:5bff:fecd:ce64' ]
    }
    **/
    // DEV: Notify listeners that we found cast devices
    if (globalApiConfig) globalApiConfig.receiverListener(chrome.cast.ReceiverAvailability.AVAILABLE);
  });

  browser.on('serviceDown', function (service) {
    var downReceiver = new chrome.cast.Receiver(service.fullname, service.name);
    if (typeof service.addresses !== 'undefined' && Array.isArray(service.addresses) && service.addresses.length > 0) {
      downReceiver.ipAddress = service.addresses[0];
    } else {
      downReceiver.ipAddress = false;
    }
    receiverList = receiverList.filter(function (receiver) {
      return (receiver.ipAddress === false || receiver.ipAddress !== downReceiver.ipAddress) && receiver.name !== downReceiver.name && receiver.friendlyName !== downReceiver.friendlyName;
    });
    // DEV: If we have run out of receivers, notify listeners that there are none available
    if (receiverList.length === 0) globalApiConfig.receiverListener(chrome.cast.ReceiverAvailability.UNAVAILABLE);
  });
  if (browser.ready) {
    browser.browse();
  } else {
    browser.once('ready', function () {
      browser.browse();
    });
  }
};

exports.default = Cast;


Cast.startBrowsing();

// Static Classes
Cast.ApiConfig = _ApiConfig2.default;
Cast.Error = _Error2.default;
Cast.Image = _Image2.default;
Cast.Receiver = _Receiver2.default;
Cast.ReceiverDisplayStatus = _ReceiverDisplayStatus2.default;
Cast.SenderApplication = _SenderApplication2.default;
Cast.Session = _Session2.default;
Cast.SessionRequest = _SessionRequest2.default;
Cast.Volume = _Volume2.default;

// Extensions
Cast.media = _media2.default;
Cast.timeout = {};