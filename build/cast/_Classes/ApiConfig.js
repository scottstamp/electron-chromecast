"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast.ApiConfig

var ApiConfig = function ApiConfig(sessionRequest, sessionListener, receiverListener, opt_autoJoinPolicy, opt_defaultActionPolicy) {
  _classCallCheck(this, ApiConfig);

  this.sessionRequest = sessionRequest;
  this.sessionListener = sessionListener;
  this.receiverListener = receiverListener;
  this.autoJoinPolicy = opt_autoJoinPolicy || chrome.cast.AutoJoinPolicy.TAB_AND_ORIGIN_SCOPED;
  this.defaultActionPolicy = opt_defaultActionPolicy || chrome.cast.DefaultActionPolicy.CREATE_SESSION;
};

exports.default = ApiConfig;