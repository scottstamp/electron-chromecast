"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.Image

var Image = function Image(url) {
  _classCallCheck(this, Image);

  this.url = url;
  this.height = null;
  this.width = null;
};

exports.default = Image;