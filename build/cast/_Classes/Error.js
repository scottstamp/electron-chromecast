"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.Error

var Error = function Error(code, opt_description) {
  var opt_details = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  _classCallCheck(this, Error);

  this.code = code;
  this.description = opt_description || null;
  this.details = opt_details;
};

exports.default = Error;