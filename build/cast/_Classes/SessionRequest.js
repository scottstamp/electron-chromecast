"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.SessionRequest

var SessionRequest = function SessionRequest(appId) {
  var opt_capabilities = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var opt_timeout = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

  _classCallCheck(this, SessionRequest);

  this.appId = appId;
  this.capabilities = opt_capabilities || [chrome.cast.Capability.VIDEO_OUT, chrome.cast.Capability.AUDIO_OUT];
  this.language = null;
  this.requestSessionTimeout = opt_timeout || chrome.cast.timeout.requestSession;
};

exports.default = SessionRequest;