"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.SenderApplication

var SenderApplication = function SenderApplication(platform) {
  _classCallCheck(this, SenderApplication);

  this.platform = platform;

  // TODO: Determine usage of these two properties
  this.packageId = null;
  this.url = null;
};

exports.default = SenderApplication;