"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.ReceiverDisplayStatus

var ReceiverDisplayStatus = function ReceiverDisplayStatus(statusText, appImages) {
  _classCallCheck(this, ReceiverDisplayStatus);

  this.statusText = statusText;
  this.appImages = appImages;
};

exports.default = ReceiverDisplayStatus;