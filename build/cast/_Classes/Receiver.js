"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.Receiver

var Receiver = function Receiver(label, friendlyName) {
  var opt_capabilities = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var opt_volume = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

  _classCallCheck(this, Receiver);

  this.capabilities = opt_capabilities || [chrome.cast.Capability.VIDEO_OUT, chrome.cast.Capability.AUDIO_OUT];
  this.displayStatus = null;
  this.friendlyName = friendlyName;
  this.label = label;
  this.volume = opt_volume;

  this.receiverType = chrome.cast.ReceiverType.CAST;

  // DEV: Properties not in spec but in crx
  this.ipAddress = null;
  this.isActiveInput = true;
};

exports.default = Receiver;