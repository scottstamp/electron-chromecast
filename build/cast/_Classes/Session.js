'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); // https://developers.google.com/cast/docs/reference/chrome/chrome.cast.Session


var _castv = require('castv2');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Session = function () {
  function Session(sessionId, appId, displayName, appImages, receiver, _cb) {
    var _this = this;

    _classCallCheck(this, Session);

    this.clientConnection = null;
    this.clientHeartbeat = null;
    this.clientReceiver = null;

    this.client = new _castv.Client();
    this.client.on('message', castConsole.warn.bind(castConsole));
    this.client.connect({
      host: receiver.ipAddress,
      port: receiver.port
    }, function () {
      var transportHeartbeat = void 0;
      // create various namespace handlers
      _this.clientConnection = _this.client.createChannel('sender-0', 'receiver-0', 'urn:x-cast:com.google.cast.tp.connection', 'JSON');
      _this.clientHeartbeat = _this.client.createChannel('sender-0', 'receiver-0', 'urn:x-cast:com.google.cast.tp.heartbeat', 'JSON');
      _this.clientReceiver = _this.client.createChannel('sender-0', 'receiver-0', 'urn:x-cast:com.google.cast.receiver', 'JSON');

      // establish virtual connection to the receiver
      _this.clientConnection.send({ type: 'CONNECT' });
      window.addEventListener('beforeunload', function () {
        _this.clientConnection.send({ type: 'CLOSE' });
        if (_this.transportConnect) {
          _this.transportConnect.send({ type: 'CLOSE' });
        }
      });

      // start heartbeating
      _this.clientHeartbeat.send({ type: 'PING' });
      _this.clientHeartbeatInterval = setInterval(function () {
        try {
          if (transportHeartbeat) {
            transportHeartbeat.send({ type: 'PING' });
          }
          _this.clientHeartbeat.send({ type: 'PING' });
        } catch (e) {
          _this._triggerConnectionError();
        }
      }, 5000);

      // launch appId
      castConsole.info('LAUNCHING: ' + appId);
      _this.clientReceiver.send({ type: 'LAUNCH', appId: appId, requestId: 1 });

      // display receiver status updates
      var once = true;
      _this.clientReceiver.on('message', function (data, broadcast) {
        // eslint-disable-line no-unused-vars
        // TODO: Implement broadcast, remove disabled eslint
        if (data.type === 'RECEIVER_STATUS') {
          if (data.status.applications && data.status.applications[0].appId !== appId) {
            _this.status = chrome.cast.SessionStatus.STOPPED;
            _this.client.close();
            clearInterval(_this.clientHeartbeatInterval);
            _this._triggerConnectionError();
            return;
          }
          if (data.status.applications && data.status.applications[0].appId === appId) {
            var app = data.status.applications[0];
            _this.app = app;
            if (once) {
              once = false;
              _this.transport = _this.transportId = app.transportId;
              _this.clientId = 'client-' + Math.floor(Math.random() * 10e5);
              _this.transportConnect = _this.client.createChannel(_this.clientId, _this.transport, 'urn:x-cast:com.google.cast.tp.connection', 'JSON');
              _this.transportConnect.send({ type: 'CONNECT' });
              transportHeartbeat = _this.client.createChannel(_this.clientId, _this.transport, 'urn:x-cast:com.google.cast.tp.heartbeat', 'JSON');
              _this.status = chrome.cast.SessionStatus.CONNECTED;
              _this.sessionId = app.sessionId;
              _this.namespaces = app.namespaces;
              _this.displayName = app.displayName;
              _this.statusText = app.displayName;

              // DEV: Media Listener
              // TODO: Move somewhere nicer
              _this.addMessageListener('urn:x-cast:com.google.cast.media', function (namespace, media) {
                if (media.status && media.status.length > 0) {
                  castConsole.info('Media Reciever', media);
                  castConsole.error('Media Reciever', _this._mediaHooks);
                  var mediaObject = new chrome.cast.media.Media(_this.app.sessionId, media.requestId, _this._channels['urn:x-cast:com.google.cast.media']);
                  mediaObject._hydrate(media);
                  _this._mediaHooks.forEach(function (hookFn) {
                    return hookFn(mediaObject);
                  });
                }
              });
              _cb(_this);
            }

            _this.statusText = app.statusText;
            _this.displayName = app.displayName;
            _this.namespaces = app.namespaces;
          }
          if (data.status.volume) {
            _this.receiver.volume = new chrome.cast.Volume(data.status.volume.level, data.status.volume.muted);
          }
          castConsole.info('Firing Update Hooks');
          _this._updateHooks.forEach(function (hook) {
            hook(true);
          });
          castConsole.log('Reciever Update:', data.status);
        } else {
          castConsole.info('RANDOM MSG', data);
        }
      });
    });

    this.appId = appId;
    this.appImages = appImages;
    this.displayName = displayName;
    this.receiver = receiver;
    this.sessionId = sessionId;

    this.media = [];
    this.namespaces = [];
    this.senderApps = [];
    this.status = chrome.cast.SessionStatus.DISCONNECTED;
    this.statusText = null;

    this._mediaHooks = [];
    this._channels = {};
    this._updateHooks = [];

    this.sequenceNumber = 0;
  }

  _createClass(Session, [{
    key: '_triggerConnectionError',
    value: function _triggerConnectionError() {
      // Session is not properly connected anymore call the hook with false to signal issue
      castConsole.info('Issue with connection');
      this._updateHooks.forEach(function (hook) {
        hook(false);
      });
    }
  }, {
    key: '_createChannel',
    value: function _createChannel(namespace) {
      if (!this._channels[namespace]) {
        this._channels[namespace] = this.client.createChannel(this.clientId, this.transport, namespace, 'JSON');
        this._channels[namespace].on('message', function (message) {
          castConsole.info('Message on:', namespace, message);
        });
      }
    }
  }, {
    key: 'addMediaListener',
    value: function addMediaListener(listener) {
      castConsole.info('Media Listener: ', listener);
      this._mediaHooks.push(listener);
    }
  }, {
    key: 'addMessageListener',
    value: function addMessageListener(namespace, listener) {
      this._createChannel(namespace);
      this._channels[namespace].on('message', function (data) {
        listener(namespace, JSON.stringify(data));
      });
      castConsole.info('Message Hook For: ', namespace);
    }
  }, {
    key: 'addUpdateListener',
    value: function addUpdateListener(listener) {
      this._updateHooks.push(listener);
      castConsole.info('Update listener', listener);
    }
  }, {
    key: 'leave',
    value: function leave(successCallback, errorCallback) {
      // eslint-disable-line no-unused-vars
      // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast.Session#leave
      castConsole.info('leave');
    }
  }, {
    key: 'loadMedia',
    value: function loadMedia(loadRequest, successCallback, errorCallback) {
      var _this2 = this;

      this.sendMediaMessage({
        type: 'LOAD',
        requestId: 0,
        media: loadRequest.media,
        activeTrackIds: loadRequest.activeTrackIds || [],
        autoplay: loadRequest.autoplay || false,
        currentTime: loadRequest.currentTime || 0,
        customData: loadRequest.customData || {},
        repeatMode: 'REPEAT_OFF'
      });
      var once = true;
      castConsole.info('ADD LISTEN');
      this.addMessageListener('urn:x-cast:com.google.cast.media', function (namespace, data) {
        var mediaObject = JSON.parse(data);
        if (once && mediaObject.status && mediaObject.status.length > 0) {
          castConsole.info('LISTEN FIRED');
          once = false;
          var media = new chrome.cast.media.Media(_this2.app.sessionId, mediaObject.status[0].mediaSessionId, _this2._channels['urn:x-cast:com.google.cast.media']);
          media._hydrate(mediaObject);
          _this2.media = media;
          successCallback(media);
        } else {
          errorCallback(new chrome.cast.Error(chrome.cast.ErrorCode.SESION_ERROR));
        }
      });
      // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast.Session#loadMedia
    }
  }, {
    key: 'queueLoad',
    value: function queueLoad(queueLoadRequest, successCallback, errorCallback) {
      var _this3 = this;

      // eslint-disable-line

      var queueLoadRequestData = {
        type: 'QUEUE_LOAD',
        requestId: 0,
        customData: queueLoadRequest.customData || {},
        items: queueLoadRequest.items || [],
        repeatMode: 'REPEAT_OFF',
        startIndex: queueLoadRequest.startIndex || 0,
        sessionId: this.sessionId
      };

      queueLoadRequestData.items.forEach(function (item) {
        delete item.itemId;
        delete item.media.duration;
        delete item.media.metadata;
        delete item.media.textTrackStyle;
      });

      var once = true;
      castConsole.info('ADD LISTEN');
      this.addMessageListener('urn:x-cast:com.google.cast.media', function (namespace, data) {
        var mediaObject = JSON.parse(data);
        if (once && mediaObject.status && mediaObject.status.length > 0) {
          castConsole.info('LISTEN FIRED');
          once = false;
          var media = new chrome.cast.media.Media(_this3.app.sessionId, mediaObject.status[0].mediaSessionId, _this3._channels['urn:x-cast:com.google.cast.media']);
          media._hydrate(mediaObject);
          _this3.media = media;
          successCallback(media);
        } else if (once) {
          errorCallback(new chrome.cast.Error(chrome.cast.ErrorCode.SESION_ERROR));
        }
      });
      this.sendMediaMessage(queueLoadRequestData);

      // TODO: https://developers.google.com/cast/docs/reference/chrome/chrome.cast.Session#queueLoad
      castConsole.info('Queue Load', queueLoadRequest);
    }
  }, {
    key: 'removeMediaListener',
    value: function removeMediaListener(listener) {
      castConsole.info('Remove Media Listener');
      this._mediaHooks = this._mediaHooks.filter(function (item) {
        return item !== listener;
      });
    }
  }, {
    key: 'removeMessageListener',
    value: function removeMessageListener(namespace, listener) {
      castConsole.info('Remove Message Listener');
      if (!this._messageHooks[namespace]) return;
      this._messageHooks[namespace] = this._messageHooks[namespace].filter(function (item) {
        return item !== listener;
      });
    }
  }, {
    key: 'removeUpdateListener',
    value: function removeUpdateListener(listener) {
      castConsole.info('Remove Update Listener');
      this._updateHooks = this._updateHooks.filter(function (item) {
        return item !== listener;
      });
    }
  }, {
    key: 'sendMediaMessage',
    value: function sendMediaMessage(message) {
      var _this4 = this;

      try {
        this.sendMessage('urn:x-cast:com.google.cast.media', message, function () {}, function () {
          _this4._triggerConnectionError();
        });
      } catch (e) {
        this._triggerConnectionError();
      }
    }

    // https://developers.google.com/cast/docs/reference/chrome/chrome.cast.Session#sendMessage

  }, {
    key: 'sendMessage',
    value: function sendMessage(namespace, message, successCallback, errorCallback) {
      castConsole.info('Sending Message', namespace, message);
      this._createChannel(namespace);
      try {
        // this._channels[namespace].send({
        //   type: 'app_message',
        //   message: {
        //     sessionId: this.app.sessionId,
        //     namespaceName: namespace,
        //     message,
        //   },
        //   timeoutMillis: 3000,
        //   sequenceNumber: ++this.sequenceNumber,
        //   clientId: this.clientId,
        // });
        this._channels[namespace].send(message);
      } catch (e) {
        errorCallback(new chrome.cast.Error(chrome.cast.ErrorCode.SESSION_ERROR));
        return;
      }
      successCallback();
    }
  }, {
    key: 'setReceiverMuted',
    value: function setReceiverMuted(muted, successCallback, errorCallback) {
      try {
        var data = {
          type: 'SET_VOLUME',
          volume: { muted: muted },
          requestId: 0
        };
        this.clientReceiver.send(data);
      } catch (e) {
        errorCallback(new chrome.cast.Error(chrome.cast.ErrorCode.SESSION_ERROR));
      }
      successCallback();
    }
  }, {
    key: 'setReceiverVolumeLevel',
    value: function setReceiverVolumeLevel(newLevel, successCallback, errorCallback) {
      try {
        var data = {
          type: 'SET_VOLUME',
          volume: { level: newLevel },
          requestId: 0
        };
        this.clientReceiver.send(data);
      } catch (e) {
        errorCallback(new chrome.cast.Error(chrome.cast.ErrorCode.SESSION_ERROR));
      }
      successCallback();
    }
  }, {
    key: 'stop',
    value: function stop(successCallback, errorCallback) {
      try {
        var data = {
          type: 'STOP',
          sessionId: this.sessionId,
          requestId: 0
        };
        this.clientReceiver.send(data);
      } catch (e) {
        errorCallback(new chrome.cast.Error(chrome.cast.ErrorCode.SESSION_ERROR));
      }
      this.status = chrome.cast.SessionStatus.STOPPED;
      try {
        this.client.close();
      } catch (e) {
        // Nothing to do here
      }
      clearInterval(this.clientHeartbeatInterval);
      this._triggerConnectionError();
      successCallback();
    }
  }]);

  return Session;
}();

exports.default = Session;