"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// https://developers.google.com/cast/docs/reference/chrome/chrome.cast.Volume

var Volume = function Volume(opt_level, opt_muted) {
  _classCallCheck(this, Volume);

  this.level = opt_level || null;
  this.muted = opt_muted || null;
};

exports.default = Volume;