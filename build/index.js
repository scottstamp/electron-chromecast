'use strict';

var _cast = require('./cast');

var _cast2 = _interopRequireDefault(_cast);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

global.chrome = {
  cast: _cast2.default // eslint-disable-line
};

global.requestHandler = function (receiverList) {
  return new Promise(function (resolve) {
    return resolve(receiverList[0]);
  });
};

global.castConsole = {
  log: function log() {
    var _console;

    if (!global.castDevMode) return;
    (_console = console).log.apply(_console, arguments); // eslint-disable-line
  },
  info: function info() {
    var _console2;

    if (!global.castDevMode) return;
    (_console2 = console).info.apply(_console2, arguments); // eslint-disable-line
  },
  warn: function warn() {
    var _console3;

    if (!global.castDevMode) return;
    (_console3 = console).warn.apply(_console3, arguments); // eslint-disable-line
  },
  error: function error() {
    var _console4;

    if (!global.castDevMode) return;
    (_console4 = console).error.apply(_console4, arguments); // eslint-disable-line
  }
};

module.exports = function (requestHandler, dev) {
  global.requestHandler = requestHandler;
  global.castDevMode = dev;
};